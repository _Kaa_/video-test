package home.at.cat.video_test;

import android.content.Intent;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

public class QuestionActivityBeforeLollipop extends AppCompatActivity implements SurfaceHolder.Callback
{
    static final int REQUEST_VIDEO_CAPTURE = 1;

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_question_before_lollipop );

        Toolbar toolbar = ( Toolbar ) findViewById( R.id.toolbar );
        setSupportActionBar( toolbar );

        // TextView with question text
        TextView questionText = ( TextView ) findViewById( R.id.questionText );

        Intent intent = getIntent();
        int questionId = intent.getIntExtra( "questionId", 0 );

        if ( questionId > 0 )
        {
            switch ( questionId )
            {
                case 1:
                    questionText.setText( getResources().getString( R.string.question_1_text ) );
                    getSupportActionBar().setTitle( getResources().getString( R.string.question_1 ) );
                    break;
            }
        }

        Camera mCamera = getCameraInstance();// Create our Preview view and set it as the content of our activity.
        CameraPreview mPreview = new CameraPreview( this, mCamera );
        mCamera.setDisplayOrientation( 90 );

        FrameLayout preview = ( FrameLayout ) findViewById( R.id.camera_preview );
        preview.addView( mPreview );

        LayoutInflater controlInflater = LayoutInflater.from( getBaseContext() );
        View viewControl = controlInflater.inflate( R.layout.layout_controls, null );
        LayoutParams layoutParamsControl = new LayoutParams( LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT );

        this.addContentView( viewControl, layoutParamsControl );
    }

    public static Camera getCameraInstance()
    {
        Camera c = null;
        try
        {
            for ( int camNo = 0; camNo < Camera.getNumberOfCameras(); camNo++ )
            {
                Camera.CameraInfo camInfo = new Camera.CameraInfo();
                Camera.getCameraInfo( camNo, camInfo );

                if ( camInfo.facing == ( Camera.CameraInfo.CAMERA_FACING_FRONT ) )
                {
                    c = Camera.open( camNo );
                }
            }

            if ( c == null )
            {
                // no front-facing camera, use the first back-facing camera instead
                c = Camera.open();
            }
        }
        catch ( Exception e )
        {

        }
        return c; // returns null if camera is unavailable
    }

    @Override
    public void surfaceCreated( SurfaceHolder holder )
    {

    }

    @Override
    public void surfaceChanged( SurfaceHolder holder, int format, int width, int height )
    {

    }

    @Override
    public void surfaceDestroyed( SurfaceHolder holder )
    {

    }


    //    private void dispatchTakeVideoIntent()
    //    {
    //        Intent takeVideoIntent = new Intent( MediaStore.ACTION_VIDEO_CAPTURE );
    //        if ( takeVideoIntent.resolveActivity( getPackageManager() ) != null )
    //        {
    //            startActivityForResult( takeVideoIntent, REQUEST_VIDEO_CAPTURE );
    //        }
    //    }

}
