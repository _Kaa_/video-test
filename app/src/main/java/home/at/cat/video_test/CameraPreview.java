package home.at.cat.video_test;

import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;

import java.io.IOException;

import static android.content.ContentValues.TAG;

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback
{

    private SurfaceHolder mHolder;
    private Camera mCamera;

    CameraPreview( Context context, Camera camera )
    {
        super( context );
        mCamera = camera;

        // with a SurfaceHolder.Callback we get notified when the
        // underlying surface is created and destroyed.
        mHolder = getHolder();
        mHolder.addCallback( this );

        // deprecated setting, but required on Android versions prior to 3.0
        mHolder.setType( SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS );
    }

    public void surfaceCreated( SurfaceHolder holder )
    {
        // the Surface has been created, now tell the camera where to draw the preview.
        try
        {
            mCamera.setPreviewDisplay( holder );
            mCamera.startPreview();
        }
        catch ( IOException e )
        {
            Log.d( TAG, "Error setting camera preview: " + e.getMessage() );
        }
    }

    public void surfaceDestroyed( SurfaceHolder holder )
    {
        if ( mCamera != null )
        {
            mCamera.stopPreview();
            mCamera.release();        // release the camera for other applications
            mCamera = null;
        }
    }

    public void surfaceChanged( SurfaceHolder holder, int format, int w, int h )
    {
        // change/rotate preview
        // to stop the preview before resizing or reformatting it

        if ( mHolder.getSurface() == null )
        {
            // preview surface does not exist
            return;
        }

        // stop preview before making changes
        try
        {
            mCamera.stopPreview();
        }
        catch ( Exception e )
        {
            // ignore: tried to stop a non-existent preview
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here

        // start preview with new settings
        try
        {
            mCamera.setPreviewDisplay( mHolder );
            mCamera.startPreview();

        }
        catch ( Exception e )
        {
            Log.d( TAG, "Error starting camera preview: " + e.getMessage() );
        }
    }
}
