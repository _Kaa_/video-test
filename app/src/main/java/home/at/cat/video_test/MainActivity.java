package home.at.cat.video_test;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        // hightlight the first question
        LinearLayout question_1 = ( LinearLayout ) findViewById( R.id.question_1 );
        question_1.setBackgroundColor( getResources().getColor( R.color.blue ) );

    }

    public void askQuestion( View v )
    {
        int questionId = 0;

        // on which layout button was clicked
        LinearLayout question = ( LinearLayout ) v.getParent().getParent().getParent().getParent();

        // layout request a question
        switch( question.getId() )
        {
            case R.id.question_1:
                questionId = 1;
                break;
        }

        if( questionId > 0 )
        {
            // check version of API for appropriate Camera object
            if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP )
            {
                Intent intent = new Intent( MainActivity.this, QuestionActivityAfterLollipop.class );
                intent.putExtra( "questionId", questionId );
                startActivity( intent );
            }
            else
            {
                Intent intent = new Intent( MainActivity.this, QuestionActivityBeforeLollipop.class );
                intent.putExtra( "questionId", questionId );
                startActivity( intent );
            }
        }
    }
}
