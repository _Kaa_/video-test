package home.at.cat.video_test;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Semaphore;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class QuestionActivityAfterLollipop extends AppCompatActivity
{
    private TextureView _textureView;

    private String _cameraId;
    protected CameraDevice _cameraDevice;
    protected CameraCaptureSession _cameraCaptureSessions;
    protected CaptureRequest.Builder _captureRequestBuilder;
    private CaptureRequest.Builder _previewBuilder;

    private Size _imageDimension;
    private File _file;

    private MediaRecorder _mediaRecorder;
    private Size _videoSize;
    private String _videoAbsolutePath;
    private boolean _isRecordingVideo;
    private String _nextVideoAbsolutePath;

    private int _seconds = 45;
    private TextView _progressTimerText;
    private CountDownTimer _countDownTimer;

    private CameraCaptureSession _previewSession;

    private HandlerThread _backgroundThread;
    private Handler _backgroundHandler;

    private Semaphore _cameraOpenCloseLock = new Semaphore( 1 );

    private static final int REQUEST_CAMERA_PERMISSION = 200;
    private static final int REQUEST_AUDIO_PERMISSION = 400;

    private static final int SENSOR_ORIENTATION_DEFAULT_DEGREES = 90;
    private static final int SENSOR_ORIENTATION_INVERSE_DEGREES = 270;

    private static final String TAG = "Test Camera2";

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_question_after_lollipop );

        Toolbar toolbar = ( Toolbar ) findViewById( R.id.toolbar );
        setSupportActionBar( toolbar );

        // TextView with question text
        TextView questionText = ( TextView ) findViewById( R.id.questionText );

        Intent intent = getIntent();
        int questionId = intent.getIntExtra( "questionId", 0 );

        if ( questionId > 0 )
        {
            switch ( questionId )
            {
                case 1:
                    questionText.setText( getResources().getString( R.string.question_1_text ) );
                    getSupportActionBar().setTitle( getResources().getString( R.string.question_1 ) );
                    break;
            }
        }

        _textureView = ( TextureView ) findViewById( R.id.texture );
        assert _textureView != null;
        _textureView.setSurfaceTextureListener( textureListener );

        LayoutInflater controlInflater = LayoutInflater.from( getBaseContext() );
        View viewControl = controlInflater.inflate( R.layout.layout_controls, null );
        ViewGroup.LayoutParams layoutParamsControl = new ViewGroup.LayoutParams( ViewGroup.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT );

        this.addContentView( viewControl, layoutParamsControl );

    }

    // lifecycles of TextureView
    private TextureView.SurfaceTextureListener textureListener = new TextureView.SurfaceTextureListener()
    {
        @Override
        public void onSurfaceTextureAvailable( SurfaceTexture surface, int width, int height )
        {
            openCamera();
        }

        @Override
        public void onSurfaceTextureSizeChanged( SurfaceTexture surface, int width, int height )
        {
            //
        }

        @Override
        public boolean onSurfaceTextureDestroyed( SurfaceTexture surface )
        {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated( SurfaceTexture surface )
        {
        }
    };

    // statuses of camera device
    private final CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback()
    {
        @Override
        public void onOpened( CameraDevice camera )
        {
            _cameraDevice = camera;
            createCameraPreview();
            startTimer( _seconds );
        }

        @Override
        public void onDisconnected( CameraDevice camera )
        {
            _cameraDevice.close();
        }

        @Override
        public void onError( CameraDevice camera, int error )
        {
            _cameraDevice.close();
            _cameraDevice = null;
        }
    };

    final CameraCaptureSession.CaptureCallback captureCallbackListener = new CameraCaptureSession.CaptureCallback()
    {
        @Override
        public void onCaptureCompleted( CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result )
        {
            super.onCaptureCompleted( session, request, result );
            Toast.makeText( QuestionActivityAfterLollipop.this, "Saved:" + _file, Toast.LENGTH_SHORT ).show();
            createCameraPreview();
        }
    };

    protected void createCameraPreview()
    {
        try
        {
            SurfaceTexture texture = _textureView.getSurfaceTexture();
            assert texture != null;
            texture.setDefaultBufferSize( _imageDimension.getWidth(), _imageDimension.getHeight() );

            Surface surface = new Surface( texture );

            _captureRequestBuilder = _cameraDevice.createCaptureRequest( CameraDevice.TEMPLATE_PREVIEW );
            _captureRequestBuilder.addTarget( surface );

            _cameraDevice.createCaptureSession( Arrays.asList( surface ), new CameraCaptureSession.StateCallback()
            {
                @Override
                public void onConfigured( @NonNull CameraCaptureSession cameraCaptureSession )
                {
                    if ( null == _cameraDevice )
                    {
                        return;
                    }
                    // start to display the preview
                    _cameraCaptureSessions = cameraCaptureSession;
                    updatePreview();

                }

                @Override
                public void onConfigureFailed( @NonNull CameraCaptureSession cameraCaptureSession )
                {
                    Toast.makeText( QuestionActivityAfterLollipop.this, "Configuration change", Toast.LENGTH_SHORT ).show();
                }
            }, null );
        }
        catch ( CameraAccessException e )
        {
            e.printStackTrace();
        }
    }

    private void openCamera()
    {
        CameraManager manager = ( CameraManager ) getSystemService( Context.CAMERA_SERVICE );

        try
        {
            _cameraId = manager.getCameraIdList()[ 1 ];

            CameraCharacteristics characteristics = manager.getCameraCharacteristics( _cameraId );
            StreamConfigurationMap map = characteristics.get( CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP );
            assert map != null;
            _imageDimension = map.getOutputSizes( SurfaceTexture.class )[ 0 ];

            // add permission for camera and let user grant the permission
            if ( ActivityCompat.checkSelfPermission( this, Manifest.permission.CAMERA ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission( this, Manifest.permission.WRITE_EXTERNAL_STORAGE ) != PackageManager.PERMISSION_GRANTED )
            {
                ActivityCompat.requestPermissions( QuestionActivityAfterLollipop.this, new String[]{ Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, REQUEST_CAMERA_PERMISSION );
                return;
            }

            if ( ActivityCompat.checkSelfPermission( this, Manifest.permission.RECORD_AUDIO ) != PackageManager.PERMISSION_GRANTED )
            {
                ActivityCompat.requestPermissions( QuestionActivityAfterLollipop.this, new String[]{ Manifest.permission.RECORD_AUDIO }, REQUEST_AUDIO_PERMISSION );
                return;
            }

            _mediaRecorder = new MediaRecorder();

            _videoSize = chooseVideoSize( map.getOutputSizes( MediaRecorder.class ) );

            manager.openCamera( _cameraId, stateCallback, null );
        }
        catch ( CameraAccessException e )
        {
            e.printStackTrace();
        }

    }

    protected void updatePreview()
    {
        if ( _cameraDevice == null )
        {
            Toast.makeText( QuestionActivityAfterLollipop.this, "There is a problem with you camera", Toast.LENGTH_LONG ).show();
        }

        _captureRequestBuilder.set( CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO );

        try
        {
            _cameraCaptureSessions.setRepeatingRequest( _captureRequestBuilder.build(), null, _backgroundHandler );
        }
        catch ( CameraAccessException e )
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult( int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults )
    {
        if ( requestCode == REQUEST_CAMERA_PERMISSION )
        {
            if ( grantResults[ 0 ] == PackageManager.PERMISSION_DENIED )
            {
                // close the app
                Toast.makeText( QuestionActivityAfterLollipop.this, "Sorry!!!, you can't use this app without granting permission", Toast.LENGTH_LONG ).show();
                finish();
            }
        }
    }

    // timer to prepare / write video
    private void startTimer( final int seconds )
    {
        _progressTimerText = ( TextView ) findViewById( R.id.progressTimerText );

        if ( _countDownTimer == null )
        {
            _countDownTimer = new CountDownTimer( seconds * 1000, 1000 )
            {
                // onTick function will be called at every 1000 milliseconds

                @Override
                public void onTick( long leftTimeInMilliseconds )
                {
                    long seconds = leftTimeInMilliseconds / 1000;
                    _progressTimerText.setText( Long.toString( seconds ) );
                }

                @Override
                public void onFinish()
                {
                    _progressTimerText.setVisibility( View.GONE );
                    //startRecordingVideo();
                }
            }.start();

            _countDownTimer = null;

        }

    }

    private void setUpMediaRecorder()
    {
        try
        {
            _mediaRecorder.setAudioSource( MediaRecorder.AudioSource.MIC );
            _mediaRecorder.setVideoSource( MediaRecorder.VideoSource.SURFACE );
            _mediaRecorder.setOutputFormat( MediaRecorder.OutputFormat.MPEG_4 );

            if ( _videoAbsolutePath == null || _videoAbsolutePath.isEmpty() )
            {
                _videoAbsolutePath = getVideoFilePath();
            }

            _mediaRecorder.setOutputFile( _videoAbsolutePath );
            _mediaRecorder.setVideoEncodingBitRate( 10000000 );
            _mediaRecorder.setVideoFrameRate( 30 );
            _mediaRecorder.setVideoSize( _videoSize.getWidth(), _videoSize.getHeight() );
            _mediaRecorder.setVideoEncoder( MediaRecorder.VideoEncoder.H264 );
            _mediaRecorder.setAudioEncoder( MediaRecorder.AudioEncoder.AAC );

            _mediaRecorder.prepare();
        }
        catch( IOException ex )
        {
            ex.printStackTrace();
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();

        startBackgroundThread();

        if ( _textureView.isAvailable() )
        {
            openCamera();
        }
        else
        {
            _textureView.setSurfaceTextureListener( textureListener );
        }
    }

    @Override
    public void onPause()
    {
        //closeCamera();
        //stopBackgroundThread();
        super.onPause();
    }

    private void startBackgroundThread()
    {
        _backgroundThread = new HandlerThread( "CameraBackground" );
        _backgroundThread.start();
        _backgroundHandler = new Handler( _backgroundThread.getLooper() );
    }

    private void stopBackgroundThread()
    {
        _backgroundThread.quitSafely();
        try
        {
            _backgroundThread.join();
            _backgroundThread = null;
            _backgroundHandler = null;
        }
        catch ( InterruptedException e )
        {
            e.printStackTrace();
        }
    }

    // close camera and realese resources
    private void closeCamera()
    {
        try
        {
            _cameraOpenCloseLock.acquire();
            closePreviewSession();
            if ( null != _cameraDevice )
            {
                _cameraDevice.close();
                _cameraDevice = null;
            }
            if ( null != _mediaRecorder )
            {
                _mediaRecorder.release();
                _mediaRecorder = null;
            }
        }
        catch ( InterruptedException e )
        {
            throw new RuntimeException( "Interrupted while trying to lock camera closing." );
        }
        finally
        {
            _cameraOpenCloseLock.release();
        }
    }

    private void setUpCaptureRequestBuilder( CaptureRequest.Builder builder )
    {
        builder.set( CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO );
    }

    private String getVideoFilePath()
    {
        return getApplicationContext().getExternalFilesDir( null ).getAbsolutePath() + "/" + System.currentTimeMillis() + ".mp4";
    }

    private void startRecordingVideo()
    {
        try
        {
            //closePreviewSession();
            setUpMediaRecorder();

            SurfaceTexture texture = _textureView.getSurfaceTexture();
            assert texture != null;
            texture.setDefaultBufferSize( _textureView.getWidth(), _textureView.getHeight() );

            _previewBuilder = _cameraDevice.createCaptureRequest( CameraDevice.TEMPLATE_RECORD );
            List< Surface > surfaces = new ArrayList<>();

            // Set up Surface for the camera preview
            Surface previewSurface = new Surface( texture );
            surfaces.add( previewSurface );
            _previewBuilder.addTarget( previewSurface );

            // Set up Surface for the MediaRecorder
//            mRecorderSurface = _mediaRecorder.getSurface();
//            surfaces.add( _recorderSurface );
//            _previewBuilder.addTarget( _recorderSurface );

            // Start a capture session
            // Once the session starts, we can update the UI and start recording
            _cameraDevice.createCaptureSession( surfaces, new CameraCaptureSession.StateCallback()
            {

                @Override
                public void onConfigured( @NonNull CameraCaptureSession cameraCaptureSession )
                {
                    _previewSession = cameraCaptureSession;
                    updatePreview();
                    runOnUiThread( new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            // UI
                            _isRecordingVideo = true;

                            // start recording
                            _mediaRecorder.start();
                        }
                    } );
                }

                @Override
                public void onConfigureFailed( @NonNull CameraCaptureSession cameraCaptureSession )
                {
                    //
                }
            }, _backgroundHandler );
        }
        catch ( CameraAccessException e )
        {
            e.printStackTrace();
        }
    }

    private void closePreviewSession()
    {
        if ( _previewSession != null )
        {
            _previewSession.close();
            _previewSession = null;
        }
    }

    private void stopRecordingVideo()
    {
        // UI
        _isRecordingVideo = false;

        // stop recording
        _mediaRecorder.stop();
        _mediaRecorder.reset();

        _nextVideoAbsolutePath = null;
        //startPreview();
        createCameraPreview();
    }

    static class CompareSizesByArea implements Comparator< Size >
    {

        @Override
        public int compare( Size lhs, Size rhs )
        {
            // We cast here to ensure the multiplications won't overflow
            return Long.signum( ( long ) lhs.getWidth() * lhs.getHeight() - ( long ) rhs.getWidth() * rhs.getHeight() );
        }

    }

    private static Size chooseVideoSize( Size[] choices )
    {
        for ( Size size : choices )
        {
            if ( size.getWidth() == size.getHeight() * 4 / 3 && size.getWidth() <= 1080 )
            {
                return size;
            }
        }
        Log.e( TAG, "Couldn't find any suitable video size" );
        return choices[ choices.length - 1 ];
    }

}
